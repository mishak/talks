# Talks

Use cicero for rendering `https://cicero.xyz/v3/remark/0.14.0/github.com/mishak/talks/master/<talk>.md`.

```sh
pip install cicero==0.2.3
cicero --engine remark-0.14.0 --file <talk>
```

## 2019-11-16 [TechMeetup](https://techmeetup.cz/)

- [Guerilla DevOps](https://cicero.xyz/v3/remark/0.14.0/gitlab.com/mishak/talks/master/2019-11-16-guerilla-devops.md)
- [GitLab CI](https://cicero.xyz/v3/remark/0.14.0/gitlab.com/mishak/talks/master/2019-11-16-gitlab-ci.md)
