# DevOps odboj

## Vývojářští rebelové ve válce sysadminů

Strategie 👓, psychologie 💭 a technologie 🚀 v boji za DevOps ideály

.right[
    Michal Gebauer
]

---

# Posluchačstvo

* vývojáři

* sysadmini

### Jak vykročit

* směrem k DevOps

* zvýšení platu

* nové práci

---

# Co je to DevOps

> "Máme Bitbucket a Jenkins, jsme v DevOps celkem daleko"

> "Náš architekt dělá DevOps a hledáme k němu DevOps Engineera."

> "S architekty jsme už vyřešili DevOps, potřebujeme dodělat CI mezi OpenShift a Jenkinsem."

???

Nikdo pořádně neví co je to DevOps

---

# Co je to DevOps

**Vize jak předcházet systematickým problémům**

DevOps iniciativa je sadou hodnot, strategií a praktik

Otevřená kultura je nutnost

???

---

# Poptávka po DevOps

### 99% DevOps Engineer

### 5% DevOps Manager

### 1% Site Reliability Engineer

???

V poslední práci sem měl štěstí pracovat nebo být v kontaktu s cca 20 týmy.

Za poslední půl rok sem byl na cca 10 interview.

Všude to vypadalo přibližně stejně.

Procenta dávají dohromady 100% všechny nabídky na DevOps Manager pozici obsahují i práci DevOps Engineer pozice

---

# Reálná potřeba

### 99% ~~DevOps Engineer~~ CI/CD engineer

### 1% ~~DevOps~~ manažer

### 0.1% 🦄

???

Místo SRE je jednodušší sehnat jednorožce

---

# Ideální ~~DevOps~~ Agile tým

### 20% Domain experts

### 40% Junior Engineers

### 40% Senior Engineers / Site Reliability Engineers

Autonomní v rozhodování spolu s jejich Product Ownerem

Jasně definovaná odpovědnost, pravidla, postupy a postihy

---

# Reálný Agile tým

### 80% Junior Engineers

### 20% Senior Engineers

Mimo business zadání jen čas na záplaty

Odpovědnost za všechno, co nepřišijí sysadminům na triko

Žádná pravidla neexistují

---

# ~~Efektivní komunikace~~ Řídit IT jako stát


* CEO > CTO > ITC/ITIL/Windows/ERP/Development/Release Manager
* Development Manager > Team Leader > vývojáři
* ITC/ITIL/Windows/ERP Manager > sysadmini
* Release Manager 🔫 vývojáři/sysadmini
* Tribe Master > Product Owner > vývojáři
* představenstvo > ???
* ??? > Business Inteligence > vývojáři/sysadmini
* ??? > Marketing > vývojáři/sysadmini
* ITC Manager > tech support
* ??? > customer support
* CEO > Projektová kancelář > Project Manager > ??? > Product Owner

???

Určitě sem na někoho zapomněl

---

# Založme DevOps odboj

* ITIL je zlo

* top-down organizace je zlo

* 👵 vývojářka zná 🐋 tak zařídí Kubernetes a se *o to postará* (z toho 💩)

* 🧒 vývojáři v ☁ sami !!!

* 🧱 zazdít open-space

???

*K8s: Nejstřelenější je, že to slyšíte od manažerů nejen od vývojářů.*

Ironickým argumentem pro open-space je úspora nákladů.

---

# Cupitání za sysadminy 🍻 🍕

Začněte u ~~sebe~~ sysadminů

* aktivní ~~od~~poslech

* pomoc s ostatními týmy při schůzích

* blameless postmortem

* základy gitu

* základy programování v 🐍 nebo v 🐍 nebo v 🐍

* alert fatigue

* základy CI

???

Největší problémy pokud začínáte s DevOps budou mezi vámi a sysadminy
Zakladní věci jako komunikace

Zajděte s nimi chodit na oběd nebo pivo.

Musíte se naučit komunikovat s lidmi, kteří vám nerozumí a dívají se na vás jako na člověka co jim jen přidělává práci.

Pokuste se dívat na problémy z jejich perspektivy.

Poproste sysadminy o pomoc s vašim problémem.

"Nevím jak vyřešit X, nemáš nápad jak na to?"

"Dík za přístup k alertům."

"Trochu se v alertech ztrácím, myslíš že by jsi mi mohla pomoct je oštítkovat?"

"Hele přidali jsme `/status/db` vráti ti HTTP 200 až když migrace doběhne, mohla bys na to udělat alert?"

"Už ses vyspala po včerejším zásahu? Mohli by jsme se sejít a udělat blameless postmortem."

Požádejte o pomoc nepřicházejte s řešením.

Buďte připravení svoje řešení vyhodit oknem.

**Štouchejte pomalu ale neustále a oni si zvyknou.**

---

# Jak se zbavit korporátu

## Release Manager 🔫

* **komunikace s ostatními týmy**

* závislost na verzích jiných služeb

* migrace databáze

---

# Jak se zbavit korporátu

## Scrum Master/Team Whisperer

* ukažte jim občanku, snad už jste velcí kluci a holky

> Neseď při standupu 🤚💺

> Černý puntík za zmeškaný standup 😌

> Nevyplnila jsi kapitalizaci ticketu 😫

---

# Vykročení mimo IT;

## Business Inteligence

Nejlehčí zůsob jak zjistit cenu uptime vašeho *mikro*litu.

## Marketing

Osvoboďte člověka co se musel naučit JavaScript a provozuje servery o kterých neví ani sysadmini.

---

.right[
![IT pro vývojáře](imgs/devops-it-developers-smol.jpg)
]

# DevOps ~~Engineer~~ Saboteur

Najděte si opravdový tým 🤦‍♀️

* méně stresu

* smysluplná práce

* vývojáři vás budou mít rádi

* **daleko méně stresu**

???

Nejlepší způsob jak poznat firmu co neví jak začít dělat DevOps

Ze systematického pohledu je DevOps Engineer krátkodobá záplata, která se dlouhodobě vymstí.

---

# DevOps Manager

.center[
![x](imgs/stewie.gif)
]

???

Nejlepší způsob jak poznat firmu co si myslí že dělá DevOps

---

# DevOps bez požehnání od ~~bohů~~ boardu

Požehnání k DevOps musíte získat u představenstva nebo ředitele

Rozpočet a politické krytí ☂ před zbytkem firmy/oddělení

.center[
    ![Carrie Lam](imgs/carrie-lam-piglet-smol.jpg)
]

???

Ne nutně vy samotní, může vám v tom pomoct váš manažer nebo CTO.

Změna firemní kultury vyžaduje politický kapitál, který middle management nemá.

---

# Dotazy

@mishak87

contact@michalgebauer.com

[michalgebauer.com/slack](https://michalgebauer.com/slack)
